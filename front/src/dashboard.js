import * as React from "react";
import Datasource from "./datasource";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: [], redirect: false};
        this.ds = new Datasource();

    }

    componentDidMount() {
        const {items} = this.state;
        let self = this;
        let callback = function (data) {
            console.log(data);

            self.setState({items: data})
        };

        let id = "";
        if (this.props.match.params) {
            id = this.props.match.params.id;

            console.log("Id is " + id);
        }

        let endDt = new Date();
        endDt.setDate((endDt.getDate() + 7));

        this.ds.apiGet("/api/v1/events/", {
            start: new Date().toISOString(),
            end: endDt.toISOString(),
            id: id
        }, callback);
    }


    render() {
        const {redirect} = this.state;

        if (redirect) {
            window.location.href = "login/";
            return <h1>Wait</h1>;
        }
        if (this.state.items.length > 0)
            return (<div><h1>This is something {this.props.name}</h1>
                <List items={this.state.items}/></div>);
        else
            return <h1>This is nothing {this.props.name}</h1>
    }
}

class List extends React.Component {
    render() {
        return (
            <ul>
                {
                    this.props.items.map((item) => (
                            <p>{item.title}</p>
                        )
                    )
                }
            </ul>
        );
    }
}

export default Dashboard;
