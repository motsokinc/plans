import './App.css';
import * as React from "react";
import Welcome from "./welcome";
import Dashboard from "./dashboard";
import {Component} from "react/cjs/react.production.min";
import {Route, Switch} from "react-router";
import {Link} from "react-router-dom";
import {BrowserRouter as Router} from "react-router-dom";

class App extends Component {
  render() {
    return (
    <Router>
        <div>
          <h2>Welcome to your personal planer</h2>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav mr-auto">
            <li><Link to={'/'} className="nav-link"> Welcome </Link></li>
            <li><Link to={'/alice'} className="nav-link">Alice</Link></li>
          </ul>
          </nav>
          <hr />
          <Switch>
              <Route exact path='/' component={Welcome} />
              <Route path='/:id' component={Dashboard} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
