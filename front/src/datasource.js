class Datasource {
    apiGet(path, params, callback) {
        let url = new URL(path, window.location)
        url.search = new URLSearchParams(params).toString();

        fetch(url)
            .then((response) => {
                    if (response.status === 200)
                        return response.json();
                    else if(response.status === 403) { //forbidden
                        window.location.href = "login/";
                        return response.json();
                    }

                }
            )
            .then((data) => {
                callback(data);
            })
    }
}

export default Datasource;