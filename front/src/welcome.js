import React, { Component } from 'react';

class Welcome extends Component {
  render() {
    return (
        <div>
          <h2>Welcome, you are in a right place!</h2>
        </div>
    );
  }
}

export default Welcome;