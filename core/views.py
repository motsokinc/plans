from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import ensure_csrf_cookie


@ensure_csrf_cookie
def set_csrf_token(request):
    """
    This will be `/api/set-csrf-cookie/` on `urls.py`
    """
    return JsonResponse({"details": "CSRF cookie set"})


@login_required
def home(request):
    user = request.user
    name = '%s %s' % (user.first_name, user.last_name)

    print(request.session.items())

    # return response
    # return render(request, 'core/home.html', {'user_name': name})
    response = redirect('http://127.0.0.1:8000')
    # response.set_cookie('sessionid', request.session.session_key)

    return response
