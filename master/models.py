from django.core.serializers import json
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Master(models.Model):
    master = models.ForeignKey(User, on_delete=models.CASCADE, default=None, related_name='master')
    phone = models.CharField(max_length=20, blank=True)
    details = JSONField()  # name of service and duration
    name = models.CharField(max_length=50, blank=True, unique=True)

    def __str__(self):
        return '%s %s %s %s\n%s' % ('none' if not self.master else self.master.first_name,
                                    'none' if not self.master else self.master.last_name,
                                    'none' if not self.master else self.master.email,
                                    self.phone, self.details)

    class Meta:
        verbose_name = 'master'
        verbose_name_plural = 'masters'

