from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):
    client = models.OneToOneField(User, on_delete=models.CASCADE, related_name='client')
    phone = models.CharField(max_length=20)

    chat_id = models.IntegerField()

    def __str__(self):
        return '%s %s %s %s %d' % ('none' if not self.client else self.client.first_name,
                                   'none' if not self.client else self.client.last_name,
                                   'none' if not self.client else self.client.email,
                                   self.phone, self.chat_id)

    class Meta:
        verbose_name = 'client'
        verbose_name_plural = 'clients'
