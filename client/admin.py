from django.contrib import admin
from master.models import Master
from events.models import Event
from client.models import Client


admin.site.register(Master)
admin.site.register(Event)
admin.site.register(Client)



