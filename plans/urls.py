"""plans URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.contrib import admin
from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from django.conf.urls import url, include

from core import views as core_views
from events import views as event_views
from core.views import set_csrf_token
from django.shortcuts import render

from plans.settings import BASE_DIR


def render_react(request):
    return render(request, "index.html")


urlpatterns = [
    path('admin/', admin.site.urls),


    url(r'^after_login/', core_views.home, name='home'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(template_name=''), name='logout'),

    url(r'^oauth/', include('social_django.urls', namespace='social')),
    path('set-csrf/', set_csrf_token, name='Set-CSRF'),

    # api
    path(r'api/v1/events/', event_views.events),

    re_path(r"^$", render_react),
    re_path(r"^(?:.*)/?$", render_react),

]

LOGIN_URL = 'login'
LOGOUT_URL = 'logout'
LOGIN_REDIRECT_URL = 'home'
