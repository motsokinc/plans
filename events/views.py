from datetime import datetime
import dateutil.parser

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response

from events.event_serializer import EventSerializer
from events.models import Event


@api_view(['GET'])
@authentication_classes([SessionAuthentication])
@permission_classes([IsAuthenticatedOrReadOnly])
def events(request):
    print(request.query_params)

    st = dateutil.parser.parse(request.query_params["start"])
    e = dateutil.parser.parse(request.query_params["end"])
    name = request.query_params["id"]

    try:
        events_obj = Event.objects.filter(start__gte=st, end__lte=e, master__name=name)

    except Event.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = EventSerializer(events_obj, many=True)
        return Response(serializer.data)
