from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

from master.models import Master
from client.models import Client


class Event(models.Model):
    title = models.CharField(max_length=100, blank=True)

    start = models.DateTimeField()
    end = models.DateTimeField(default=datetime.now(), blank=True)

    master = models.ForeignKey(Master, on_delete=models.CASCADE, default=None, related_name='visits')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True, related_name='appointments')

    approved = models.BooleanField(default=False)
    remind = models.DurationField(default=None)

    def __str__(self):
        get_id = lambda obj: 'none' if obj is None else obj.id

        return '%s %s %s %s %s %s %s' % (self.title, self.start, self.end, get_id(self.master),
                                         get_id(self.client_id), self.approved, self.remind)

    class Meta:
        verbose_name = 'event'
        verbose_name_plural = 'events'
